# Given a non-empty array of integers, every element appears twice except for one. Find that single one.

# Note:
# Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?

# ex. [[x,nums.count(x)] for x in set(nums)] uses extra memory.

class Solution_1:
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        nums.sort()
        first = 0 
        last = len(nums) - 1
        while first<last:
            midpoint = (first + last)//2
            if nums[midpoint] == nums[midpoint+1]:
                if midpoint % 2 == 0:
                    first = midpoint+2
                else:
                    last = midpoint-1
            else:
                if nums[midpoint] == nums[midpoint-1]:
                    if (midpoint-1) % 2 == 0:
                        first = midpoint+1
                    else:
                        last = midpoint-2
                else:
                    return nums[midpoint]
        return nums[first]
        
# 16 / 16 test cases passed.
# Status: Accepted
# Runtime: 48 ms
# Your runtime beats 49.01 % of python3 submissions.

class Solution:
    """
    :type nums: List[int]
    :rtype: int
    """
    def singleNumber(self, nums):
        single = 0
        for i in nums:
            single ^= i
        return single
            
# 16 / 16 test cases passed.
# Status: Accepted
# Runtime: 40 ms
# Your runtime beats 99.94 % of python3 submissions.
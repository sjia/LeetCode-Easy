class Solution:
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) == 0:
            return 0
        else:
            n = nums[0]
            l = 1
            for i in range(len(nums)):
                if nums[i] != n:
                    n = nums[i]
                    nums[l] = nums[i]
                    l = l + 1
            if l < len(nums):
                nums[(l-len(nums)):] = []
            return l
            

# 161 / 161 test cases passed.
# Status: Accepted
# Runtime: 64 ms
# Your runtime beats 75.90 % of python3 submissions.
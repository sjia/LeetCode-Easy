# The isBadVersion API is already defined for you.
# @param version, an integer
# @return a bool
# def isBadVersion(version):

class Solution(object):
    def firstBadVersion(self, n):
        """
        :type n: int
        :rtype: int
        """
        if isBadVersion(1):
            return 1
        first = 1 
        last = n
        while first<last-1:
            midpoint = (first + last)//2
            if isBadVersion(midpoint):
                last = midpoint
            else:
                first = midpoint
        return last
        
# 22 / 22 test cases passed.
# Status: Accepted
# Runtime: 20 ms
# Your runtime beats 95.72 % of python submissions.
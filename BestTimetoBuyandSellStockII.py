class Solution:
    def maxProfit(self, prices):
        """
        :type prices: List[int]
        :rtype: int
        """
        n = len(prices) 
        if n == 0:
            return 0
        else:
            benefits = 0
            stock = 0
            operations = [1, -1] # buy, sell
            for i in range(n-1):
                increase = prices[i] < prices[i+1]
                if increase != stock:
                    benefits = benefits + operations[increase]*prices[i]
                stock = increase
            if stock:
                benefits = benefits + prices[n-1]
            return benefits

# idea : compare increase and stock
# 201 / 201 test cases passed.
# Status: Accepted
# Runtime: 44 ms
# Your runtime beats 89.57 % of python3 submissions.
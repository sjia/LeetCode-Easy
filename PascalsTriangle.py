class Solution(object):
    def generate(self, numRows):
        """
        :type numRows: int
        :rtype: List[List[int]]
        """
        if numRows == 0:
            return []
        else:
            Rows = [[1]]
            row = [1]
            for r in range(numRows-1):
                row.append(1)
                n_row = r+1
                for i in range(n_row//2):
                    row[n_row-1-i] = row[i] + row[i+1]
                    row[i] = row[n_row-i]
                row[n_row//2] = row[n_row - n_row//2]
                Rows.append(row[:])
            return Rows
            
# 15 / 15 test cases passed.
# Status: Accepted
# Runtime: 20 ms
# Your runtime beats 100.00 % of python submissions.
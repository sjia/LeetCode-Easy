class Solution:
    def fizzBuzz(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        result = []
        for i in range(n):
            result.append("")
            if (i+1) % 3 == 0:
                result[i]+="Fizz"
            if (i+1) % 5 == 0:
                result[i]+="Buzz"
            else:
                if (i+1) %3  != 0:
                    result[i]+=str(i+1)
        return result
        
# 8 / 8 test cases passed.
# Status: Accepted
# Runtime: 56 ms
# Your runtime beats 73.75 % of python3 submissions.
class Solution:
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        k = k%len(nums)
        t = nums[0:len(nums)-k]
        nums[0:k] = nums[-k:]
        nums[k:] = t
        
# 34 / 34 test cases passed.
# Status: Accepted
# Runtime: 52 ms
# Your runtime beats 93.33 % of python3 submissions.
class Solution:
    def containsDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        return len(nums) > len(set(nums))
        
# 18 / 18 test cases passed.
# Status: Accepted
# Runtime: 48 ms
class Solution:
    def countPrimes(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n <=2 :
            return 0
        else:
            primes = [True] * n
            primes[0] = primes[1] = False
            for i in range(n):
                if primes[i]:
                    primes[i**2:n:i] = [False] * ((n-1)//i - i + 1)
        return sum(primes)
        
# 20 / 20 test cases passed.
# Status: Accepted
# Runtime: 380 ms
# Your runtime beats 68.72 % of python3 submissions.
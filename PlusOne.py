class Solution:
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        bit = len(digits) - 1
        solved = False
        while bit >= 0 and not solved:
            if digits[bit] < 9:
                digits[bit] += 1
                solved = True
            else:
                digits[bit] = 0
                bit -= 1
        if bit == -1:
            digits.insert(0, 1)      
        return digits

# 109 / 109 test cases passed.
# Status: Accepted
# Runtime: 36 ms
# Your runtime beats 100.00 % of python3 submissions.
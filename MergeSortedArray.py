class Solution:
    def merge(self, nums1, m, nums2, n):
        """
        :type nums1: List[int]
        :type m: int
        :type nums2: List[int]
        :type n: int
        :rtype: void Do not return anything, modify nums1 in-place instead.
        """
        rtm1 = m # rtm: rest to merge
        rtm2 = n
        while rtm1>0:           
            t = nums1[rtm1-1]
            while rtm2>0 and nums2[rtm2-1] >= t:
                nums1[rtm1+rtm2-1] = nums2[rtm2-1]
                rtm2 -= 1
            nums1[rtm1+rtm2-1] = t
            rtm1 -= 1
        if rtm1 == 0:
            nums1[0:rtm2] = nums2[0:rtm2]

# 59 / 59 test cases passed.
# Status: Accepted
# Runtime: 40 ms
# Your runtime beats 79.92 % of python3 submissions.
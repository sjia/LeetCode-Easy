class Solution(object):
    def hammingWeight(self, n):
        """
        :type n: int
        :rtype: int
        """
        weight = 0
        while n>1:
            weight += n % 2
            n //= 2
        weight += n
        return weight       

# 600 / 600 test cases passed.
# Status: Accepted
# Runtime: 24 ms
# Your runtime beats 84.45 % of python submissions.